variable "region" {
  type        = "string"
  description = "Choose the region"
  default     = "us-west-2"
}

variable "web_ami" {
  type = "map"

  default {
    us-west-2 = "ami-6cd6f714"
  }
}

variable "instance_type" {
  default = "t2.micro"
}

variable "azss" {
  default = "data.aws_availability_zones.azs.name"
}

# data "aws_availability_zones" "azs" {}

variable "subscriptions" {
  description = "List of telephone numbers to subscribe to SNS."
  type        = "list"
  default     = ["+91879042094", "+919553915388"]
}

variable "key_name" {
  default = "oregon"
}

variable "ins_tag" {
  default = "web-ins"
}

variable "alarm_name" {
  default = "ec2-alarm"
}

variable "comparison_operator" {
  default = "GreaterThanOrEqualToThreshold"
}

variable "evaluation_periods" {
  default = "5"
}

variable "namespace" {
  default = "AWS/EC2"
}

variable "metric_name" {
  default = "CPUUtilization"
}

variable "period" {
  default = "60"
}

variable "statistic" {
  default = "Average"
}

variable "threshold" {
  default = "0"
}

variable "alarm_description" {
  default = "This metric monitors ec2 cpu utilization"
}

variable "protocol" {
  default = "sms"
}

variable "raw_message" {
  default = "true"
}

variable "SNS_name" {
  default = "SNS"
}

variable "InstanceId" {
  default = "{aws_instance.web.id}"
}

variable "alarm_actions" {
  default = "{aws_sns_topic.SNS.arn}"
}
