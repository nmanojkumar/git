resource "aws_instance" "web" {
  ami               = "${lookup(var.web_ami,var.region)}"
  instance_type     = "${var.instance_type}"
  availability_zone = "${data.aws_availability_zones.azs.names[count.index]}"
  key_name          = "${var.key_name}"

  tags {
    Name = "${var.ins_tag}"
  }
}

resource "aws_cloudwatch_metric_alarm" "task-alarm" {
  alarm_name          = "${var.alarm_name}"
  comparison_operator = "${var.comparison_operator}"
  evaluation_periods  = "${var.evaluation_periods}"
  metric_name         = "${var.metric_name }"
  namespace           = "${var.namespace }"
  period              = "${var.period }"
  statistic           = "${var.statistic }"
  threshold           = "${var.threshold }"
  alarm_description   = "${var.alarm_description }"
  alarm_actions       = ["${aws_sns_topic.SNS.arn}"]

  dimensions {
    InstanceId = "${aws_instance.web.id}"
  }
}

output "web" {
  value = "${aws_instance.web.id}"
}
