provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {
    bucket = "git.jenkins"
    key    = "code"
    region = "us-west-2"
  }
}
